<?php
/**
 * Prueba Tecnica DataCRM
 *
 * @author Santiago Hurtado Infante
 *
 */

use App\System\Request;

require './vendor/autoload.php';

/**
 * Class Index
 */
class Index
{

    /**
     * Index constructor.
     * @param $req
     */
    public function __construct($req)
    {
        try {
            $request = empty($req['view']) ? new Request() : new Request($req['view']);
            $request->execute();
        } catch (Throwable $th) {
            die('ERROR_STARTING_APP: ' . $th->getMessage());
        }
    }
}

new Index($_GET);