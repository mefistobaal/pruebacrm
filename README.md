# **Prueba Técnica para DataCRM**

#### Autor: Santiago Hurtado Infante
#### Email: mefistobaal96@gmail.com


Esta es la prueba tecnica para el puesto de desarrollador en la empresa DataCRM.

# Requerimientos
1. PHP 7.4+
2. [Composer](https://getcomposer.org/)
3. Apache

# Instalación
1. Clonar este repositorio dentro de la carpeta del servidor
2. Ejecutar ``composer install`` para instalar la dependencia necesaria
3. Ejecutar ``composer dumpautoload -o`` para cargar las clases de forma dinamica simulando un ambiente de producción
4. Navegar en la url de su localhost hasta el proyecto