<main role="main" class="flex-shrink-0">
    <div class="container">
        <h1 class="mt-5">Prueba Técnica DataCRM consumo API</h1>
        <p class="lead">Para ejecutar el consumo del API, de click en el botón que se encuentra en la parte
            superior.</p>
        <div class="alert alert-info alert-dismissible fade show" role="alert" id="alertLoading" hidden>
            <h4 class="alert-heading">Cargando la información...!</h4>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="alert alert-success alert-dismissible fade show" role="alert" id="alertComplete" hidden>
            <h4 class="alert-heading">Completado!</h4>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    </div>
    <div class="container" id="tableContainer" hidden>
        <table class="table">
            <thead class="thead-dark">
            <tr>
                <th scope="col">id</th>
                <th scope="col">contact_no</th>
                <th scope="col">lastname</th>
                <th scope="col">createdtime</th>
            </tr>
            </thead>
            <tbody id="tableContent">

            </tbody>
        </table>
    </div>
</main>