function getData() {
    const alertComplete = document.getElementById('alertComplete')
    const alertLoading = document.getElementById('alertLoading')
    alertComplete.setAttribute('hidden', '')
    alertLoading.removeAttribute('hidden')
    fetch('api/webservice').then(response => {
        if (response.ok)
            return response.json()
    }).then(results => {
        let data = '';
        results.forEach(result => {
            data += `<tr>
                        <th scope="row">${result.id}</th>
                        <td>${result.contact_no}</td>
                        <td>${result.lastname}</td>
                        <td>${result.createdtime}</td>
                    </tr>`
        })
        document.getElementById('tableContent').innerHTML = data
        alertLoading.setAttribute('hidden', '')
        alertComplete.removeAttribute('hidden')
        document.getElementById('tableContainer').removeAttribute('hidden')
    });
}