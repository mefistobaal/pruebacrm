<?php


namespace App\Helpers;


use Throwable;

trait Inflector
{
    static function lowerCamelCase(string $string)
    {
        try {
            return lcfirst(static::upperCamelCase($string));
        } catch (Throwable $th) {
            die('ERROR_LOWER_CAMEL_CASE: ' . $th->getMessage());
        }
    }

    static function upperCamelCase(string $string)
    {
        try {
            /** Limpieza de guiones en el nombre */
            $segments = explode('-', $string);

            /** Se recorre el array y se renombran las claves para tener Mayusculas en el Primer Caracter */
            array_walk($segments, function (&$value) {
                $value = ucfirst($value);
            });

            /** Retorno de valores concatenados */
            return implode('', $segments);
        } catch (Throwable $th) {
            die('ERROR_UPPER_CAMEL_CASE: ' . $th->getMessage());
        }
    }


}