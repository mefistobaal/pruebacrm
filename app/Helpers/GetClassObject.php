<?php

namespace App\Helpers;

use Throwable;

trait GetClassObject
{
    function getClassObject()
    {
        $controllerClassName = $this->getControllerClassName();
        $controllerFileName  = $this->getControllerFileName();
        $methodName          = $this->getMethodName();
        $params              = $this->getParameters();
        return compact('controllerClassName', 'controllerFileName', 'methodName', 'params');
    }

    function getControllerClassName()
    {
        return Inflector::upperCamelCase($this->getController());
    }

    function getControllerFileName()
    {
        try {
            /** Se resuelve la direccion del controlador */
            if (file_exists(__DIR__ . '/../Controllers/' . $this->getControllerClassName() . '.php')) {
                return dirname(__DIR__) . '/Controllers/' . $this->getControllerClassName() . '.php';
            }
        } catch (Throwable$th) {
            die('ERROR_GET_CONTROLLER_FILENAME: ' . $th->getMessage());
        }
    }

    function getMethodName()
    {
        return Inflector::lowerCamelCase($this->getMethod());
    }
}