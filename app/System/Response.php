<?php


namespace App\System;


interface Response
{
    public function execute();
}