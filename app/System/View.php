<?php


namespace App\System;


use Throwable;

class View implements Response
{
    private $view_route = __DIR__ . '/../../resources/views';
    private $view;
    private $params     = array();

    public function __construct($view = null, $params = array())
    {
        $this->__set('view', $view);
        $this->__set('params', $params);
    }

    public function execute()
    {
        try {
            $view   = $this->__get('view');
            $params = $this->__get('params');
            call_user_func(function () use ($view, $params) {
                extract($params);
                include_once $this->view_route . '/layouts/header.php';
                if (file_exists($this->view_route . "/$view.vista.php")) {
                    require_once $this->view_route . "/$view.vista.php";
                }
                include_once $this->view_route . '/layouts/footer.php';
                include_once $this->view_route . '/layouts/scripts.php';
            });
        } catch (Throwable $th) {
            die('ERROR_EXECUTE_VIEW: ' . $th->getMessage());
        }
    }

    public function __get($name)
    {
        return $this->$name;
    }

    public function __set($name, $value)
    {
        $this->$name = $value;
    }
}