<?php


namespace App\System;


use App\Helpers\GetClassObject;
use App\Helpers\Inflector;
use Throwable;

class Request
{
    protected $url;
    protected $controller;
    protected $default_controller = 'Home';
    protected $method;
    protected $default_method     = 'index';
    protected $parameters;

    use GetClassObject, Inflector;

    /**
     * Request constructor.
     * @param $url
     */
    public function __construct($url = null)
    {
        $this->setUrl($url);
        $this->buildRequest();
    }

    private function buildRequest()
    {
        try {
            /** @var  $segments String Crea los segmentos de la url para obtener los valores divididos por "/" */
            $segments = explode('/', $this->getUrl());

            /** Resolver lo valores de la construccion */
            $this->setController($segments);
            $this->setMethod($segments);
            $this->setParameters($segments);

        } catch (Throwable $th) {
            die('ERROR_BUILDING_REQUEST: ' . $th->getMessage());
        }
    }

    /**
     * @return mixed
     */
    protected function getUrl()
    {
        return $this->url;
    }

    /**
     * @param mixed $url
     */
    protected function setUrl($url)
    {
        $this->url = $url;
    }

    public function execute()
    {
        /**
         * Se obtiene el objeto de clase del Trait GetClassObject
         * @var $controllerFileName
         * @var $controllerClassName
         * @var $methodName
         * @var $params
         */
        extract($this->getClassObject());
        if (file_exists($controllerFileName)) {
            require $controllerFileName;
            $controller = new $controllerClassName();
            $response   = call_user_func_array([$controller, $methodName], $params);
            $this->execResponse($response);
        }
    }

    public function execResponse(&$response)
    {
        if ($response instanceof Response) {
            $response->execute();
        } elseif (is_array($response)) {
            http_response_code(200);
            echo json_encode($response);
        } else {
            var_dump($response);
        }
    }

    /**
     * @return mixed
     */
    protected function getController()
    {
        return $this->controller;
    }

    /**
     * @param mixed $controller
     * Se pasa por referencia el valor de $controller para modificar el valor original
     */
    protected function setController(&$controller)
    {
        /** Se guarda la primera posicion del arreglo obtenido en $controller */
        $this->controller = array_shift($controller);
        /** Se evalua la existencia del contenido de $this->controller, sino existe, se asigna el controlador por
         * defecto, de lo contrario, se escapan los datos obtenidos de la url
         */
        $this->controller = empty($this->controller) ? $this->default_controller : htmlentities(addslashes($this->controller));
    }

    /**
     * @return string
     */
    protected function getDefaultController(): string
    {
        return $this->default_controller;
    }

    /**
     * @return mixed
     */
    protected function getMethod()
    {
        return $this->method;
    }

    /**
     * @param mixed $method
     * Se pasa por referencia el valor de $method para modificar el valor original
     */
    protected function setMethod(&$method)
    {
        /** Se guarda la primera posicion del arreglo obtenido en $method */
        $this->method = array_shift($method);
        /** Se evalua la existencia del contenido de $this->method, sino existe, se asigna el metodo por
         * defecto, de lo contrario, se escapan los datos obtenidos de la url
         */
        $this->method = empty($this->method) ? $this->default_method : htmlentities(addslashes($this->method));
    }

    /**
     * @return mixed
     */
    protected function getDefaultMethod()
    {
        return $this->default_method;
    }

    /**
     * @return mixed
     */
    protected function getParameters()
    {
        return $this->parameters;
    }

    /**
     * @param mixed $parameters
     */
    protected function setParameters(&$parameters)
    {
        /** Se guarda lo restante del arreglo obtenido de la variable original $parameters */
        $this->parameters = $parameters;
    }
}