<?php


use App\Controllers\Controller;
use GuzzleHttp\Client;

class Api extends Controller
{
    private $client;
    private $token;
    private $accessKey;
    private $sessionName;

    public function __construct()
    {
        $this->client = new Client(
            [
                'base_uri' => 'https://developold.datacrm.la/datacrm/pruebatecnica/webservice.php'
            ]
        );
    }

    public function index()
    {
        return array('error' => 'Not allowed');
    }

    public function webservice()
    {
        $this->__set('token', $this->getChallenge()->result->token);
        $this->__set('accessKey', $this->createAccessKey());
        $this->__set('sessionName', $this->login()->result->sessionName);
        return $this->getDataTable()->result;
    }

    private function getChallenge()
    {
        $response = $this->client->request('GET', '', [
            'query' => [
                'operation' => 'getchallenge',
                'username'  => 'prueba',
            ]
        ])->getBody()->getContents();
        return json_decode($response);
    }

    private function createAccessKey()
    {
        return md5($this->__get('token') . '9zIDkLTMWtSMmlnh');
    }

    public function __get($name)
    {
        return $this->$name;
    }

    public function __set($name, $value)
    {
        $this->$name = $value;
    }

    private function login()
    {
        $response = $this->client->request('POST', '', [
            'form_params' => [
                'operation' => 'login',
                'username'  => 'prueba',
                'accessKey' => $this->__get('accessKey')
            ]
        ])->getBody()->getContents();
        return json_decode($response);
    }

    private function getDataTable()
    {
        $response = $this->client->request('GET', '', [
            'query' => [
                'operation'   => 'query',
                'sessionName' => $this->__get('sessionName'),
                'query'       => 'select * from Contacts;'
            ]
        ])->getBody()->getContents();
        return json_decode($response);
    }
}