<?php


use App\Controllers\Controller;
use App\System\View;

class Home extends Controller
{

    public function index()
    {
        return new View('home', ['title' => 'Santiago Hurtado']);
    }
}